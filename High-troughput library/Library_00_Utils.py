import dgrec
from tqdm import tqdm
from collections import defaultdict, Counter
import itertools
from Bio import SeqIO
from Bio.Seq import Seq
import gzip as gz
import numpy as np
import csv
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import pickle

def pickle_save(data_in, file_name_out):
    """
    Saves the input data to a file using the pickle module.
    
    Parameters:
    data_in : any Python object
        The data that you want to save (serialize).
    file_name_out : str
        The name of the file where the data will be saved. The file is opened in write-binary mode ("wb").
    """
    
    # Open a file in binary write mode ("wb")
    pickle_out = open(file_name_out, "wb")
    
    # Use pickle to serialize the data and write it to the file
    pickle.dump(data_in, pickle_out)
    
    # Close the file after writing the data
    pickle_out.close()



def pickle_load(file_name_in):
    """
    Loads and returns data from a pickle file.
    
    Parameters:
    file_name_in : str
        The name of the file (including the path, if necessary) to read the data from.
        
    Returns:
    data_out : any Python object
        The data that was saved in the pickle file, deserialized into a Python object.
    """
    
    # Open the file in binary read mode ("rb")
    pickle_in = open(file_name_in, "rb")
    
    # Use pickle to deserialize the data from the file
    data_out = pickle.load(pickle_in)
    
    # Return the deserialized data
    return data_out



def if_postive_return_plus(number):
    """
    Converts a number to a string with a plus sign if the number is positive.

    Parameters:
    number : int
        The integer to be formatted.

    Returns:
    str
        The formatted string with a plus sign for positive numbers, or the number as a string if non-positive.
    """
    # Check if the number is greater than zero
    if number > 0:
        # If positive, prepend a '+' sign to the number and return it as a string
        return '+' + str(number)
    else:
        # If zero or negative, return the number as a string without any sign
        return str(number)



def get_absolute_file_paths_with_extension(path, extension):
    """
    Recursively retrieves absolute paths of files with a specific extension within a directory.
    
    Args:
        path (str): The directory path to start the search from.
        extension (str): The file extension to filter by (e.g., '.txt', '.jpg').

    Returns:
        list: A list of absolute file paths with the specified extension.
    """
    file_paths = []  # Initialize an empty list to store the file paths

    # Walk through the directory tree using os.walk
    for folder_path, _, file_names in os.walk(path):
        for file_name in file_names:
            # Check if the file has the specified extension
            if file_name.endswith(extension):
                # if "Undetermined" not in file_name:
                absolute_path = os.path.join(folder_path, file_name)
                absolute_path="/".join(absolute_path.split('\\'))
                file_paths.append(absolute_path)

    return file_paths



def revcomp(seq):
    """
    Returns the reverse complement of a DNA sequence.
    
    Parameters:
    seq : str or any object that can be converted to a string
        The DNA sequence that you want to reverse complement.
        
    Returns:
    str
        The reverse complement of the input DNA sequence.
    """
    
    # Convert the input to a string, if it's not already a string
    seq = str(seq)
    
    # Create a translation table for complementing nucleotides
    # "A" ↔ "T", "T" ↔ "A", "G" ↔ "C", "C" ↔ "G"
    table = str.maketrans("ATGCatgc", "TACGtacg")
    
    # Translate the sequence using the complement table and reverse it
    return seq.translate(table)[::-1]



def is_geno_dgrec(geno):
    """
    Determines if a given genotype string meets a specific criterion related to the frequency of 'A' mutations.

    Parameters:
    - geno (str): A string representing a genotype, where mutations are separated by commas. Each mutation is represented as a string where the first character is the original base.

    Returns:
    - bool: True if more than 60% of the mutations in the genotype are 'A' mutations; otherwise, False.
    """
    
    # Split the genotype string into individual mutations
    geno_split = geno.split(',')

    # Initialize a counter for 'A' mutations
    count_A = 0

    # Iterate over each mutation in the split genotype string
    for k in geno_split:
        # Extract the original base from the mutation string
        old_base = k[0]
        
        # Check if the original base is 'A' and if there is more than one mutation
        if old_base == "A" and len(geno_split) > 1:
            count_A += 1

    # Calculate the proportion of 'A' mutations and check if it exceeds 60%
    return (count_A / len(geno_split)) > 0.6



def get_mutation_percentage(list_geno):
    """
    Calculates various statistics related to mutation percentages based on a list of genotypes.

    Parameters:
    - list_geno (list of tuples): A list where each tuple contains a genotype string and its count. 
      - The genotype string is a comma-separated list of mutations.
      - The count is the number of occurrences of this genotype.

    Returns:
    - tuple: A tuple containing:
      - count_total: The total number of genotype occurrences.
      - total_base_count: The total number of bases considered (assuming 70 bases per genotype).
      - count_muta_geno: The total number of mutated genotypes.
      - count_muta_base: The total number of mutated bases.
      - count_dgrec_geno: The number of genotypes classified as 'dgrec' (based on the `is_geno_dgrec` function).
      - count_dgrec_base: The total number of bases for 'dgrec' mutated bases.
    """
    
    # Initialize counters for total counts and specific categories
    count_total = 0
    count_muta_geno = 0
    count_muta_base = 0
    count_dgrec_geno = 0
    count_dgrec_base = 0
    
    # Iterate over each genotype and its count in the list
    for k in list_geno:
        count_total += k[1]  # Add to the total count of genotypes
        
        if k[0] == '':
            # If the genotype string is empty, count it as the wild-type (wt)
            count_wt = k[1]
        elif is_geno_dgrec(k[0]):
            # If the genotype is classified as 'dgrec', update respective counts
            count_dgrec_geno += k[1]
            count_dgrec_base += len(k[0].split(',')) * k[1]
            
            count_muta_geno += k[1]
            count_muta_base += len(k[0].split(',')) * k[1]
        else:
            # If the genotype is not classified as 'dgrec', update mutation counts
            count_muta_geno += k[1]
            count_muta_base += len(k[0].split(',')) * k[1]

    # Return a tuple with various calculated counts
    return (count_total, 70 * count_total, count_muta_geno, count_muta_base, count_dgrec_geno, count_dgrec_base)



def get_mutation_stats(list_geno):
    """
    Computes statistics on mutation data from a list of genotypes.
    
    Parameters:
    list_geno : list of tuples
        Each element in the list is a tuple where:
        - The first element (k[0]) is a string representing a genotype (which may be an empty string for wild type).
        - The second element (k[1]) is an integer representing the count of occurrences for that genotype.
        
    Returns:
    tuple
        A tuple of the following statistics:
        - count_total : Total number of occurrences across all genotypes.
        - base_total : The total count scaled by 70 (length of a TR).
        - count_muta_geno : Total number of occurrences of genotypes with mutations.
        - count_muta_base : Total number of mutated nucleotide positions across all genotypes with mutations.
        - count_dgrec_geno : Total number of occurrences of 'DGR' genotypes  according to the `is_geno_dgrec` function.
        - count_dgrec_base : Total number of degenerate base positions across all 'DGR' genotypes.
    """

    # Initialize counters
    count_total = 0  # Total number of occurrences across all genotypes
    
    count_muta_geno = 0  # Total number of occurrences of genotypes with mutations
    count_muta_base = 0  # Total number of mutated base positions across all genotypes
    
    count_dgrec_geno = 0  # Total number of occurrences of 'DGR' genotypes
    count_dgrec_base = 0  # Total number of 'DGR' base positions

    # Loop over each genotype in the input list
    for k in list_geno:
        # Add the count of occurrences of the current genotype to the total
        count_total += k[1]
        
        if k[0] == '':  # If the genotype is empty, treat it as wild type
            count_wt = k[1]
        elif is_geno_dgrec(k[0]):  # If the genotype is 'DGR'
            # Update counts for 'DGR' genotypes
            count_dgrec_geno += k[1]
            count_dgrec_base += len(k[0].split(',')) * k[1]
            
            # Also count these as mutated genotypes
            count_muta_geno += k[1]
            count_muta_base += len(k[0].split(',')) * k[1]
            
        else:  # For non 'DGR' mutated genotypes 
            # Update counts for mutated genotypes
            count_muta_geno += k[1]
            count_muta_base += len(k[0].split(',')) * k[1]

    # Return the computed statistics as a tuple
    return (count_total, 70 * count_total, count_muta_geno, count_muta_base, count_dgrec_geno, count_dgrec_base)



def genotyping_dataframe(df):
    """
    Generates a DataFrame with mutation statistics for each template in a given pandas.DataFrame.
    
    Parameters:
    df : pandas.DataFrame with columns:
    -'TR'
    -'geno_list'        

    Returns:
    pandas.DataFrame
        A DataFrame containing mutation statistics for each template.
        Columns include:
        - 'TR': The template identifier.
        - 'geno_list': List of genotypes for the template.
        - 'count_total_geno': Total number of occurrences of all genotypes for the template.
        - 'count_total_base': Total number of base positions, scaled.
        - 'count_muta_geno': Number of occurrences of mutated genotypes.
        - 'count_muta_base': Number of mutated base positions.
        - 'count_dgrec_geno': Number of occurrences of degenerate genotypes.
        - 'count_dgrec_base': Number of degenerate base positions.
    """
    
    # Initialize empty lists to store data for each column
    TR_list = []  # List of template identifiers
    count_total_geno_list = []  # Total number of occurrences of all genotypes
    count_total_base_list = []  # Total number of base positions, scaled by 70
    count_muta_geno_list = []  # Number of occurrences of mutated genotypes
    count_muta_base_list = []  # Number of mutated base positions
    count_dgrec_geno_list = []  # Number of occurrences of degenerate genotypes
    count_dgrec_base_list = []  # Number of degenerate base positions
    geno_list_list = []  # List of genotype lists for each template

    # Loop over each template in the 'TR' column of the dictionary/dataframe
    for template in tqdm(list(df['TR'])):
        # Extract the genotype list for the current template
        geno_list = df[df['TR'] == template]['geno_list'].iloc[0]
        # Convert the genotype list string to an actual list (if needed)
        # geno_list = string_to_tuple_list(geno_list)  # Uncomment if genotypes are stored as strings
        
        # Get mutation statistics for the current genotype list
        count_total_geno, count_total_base, count_muta_geno, count_muta_base, count_dgrec_geno, count_dgrec_base = get_mutation_stats(geno_list)

        # Append data to respective lists
        TR_list.append(template)
        count_total_geno_list.append(count_total_geno)
        count_total_base_list.append(count_total_base)
        count_muta_geno_list.append(count_muta_geno)
        count_muta_base_list.append(count_muta_base)
        count_dgrec_geno_list.append(count_dgrec_geno)
        count_dgrec_base_list.append(count_dgrec_base)
        geno_list_list.append(geno_list)

    # Create a DataFrame from the collected data
    df_muta_stats = pd.DataFrame({
        'TR': TR_list,
        'geno_list': geno_list_list,
        'count_total_geno': count_total_geno_list,
        'count_total_base': count_total_base_list,
        'count_muta_geno': count_muta_geno_list,
        'count_muta_base': count_muta_base_list,
        'count_dgrec_geno': count_dgrec_geno_list,
        'count_dgrec_base': count_dgrec_base_list
    })
    
    return df_muta_stats  # Return the resulting DataFrame



def genotype_dataframe_parents(dic_match):
    """
    Generates a DataFrame containing mutation statistics for each template in the provided dictionary.

    Parameters:
    - dic_match (dict): A dictionary generated by the function 'match_parent_with_muta_with_outside':
      - Keys are parents molecules.
      - Values are mutagenized molecules.

    Returns:
    - df_muta_stats (pandas.DataFrame): A DataFrame with columns for mutation statistics and genotype lists for each template.
    """
    
    # Initialize lists to store data for DataFrame columns
    TR_list = []
    count_total_geno_list = []
    count_total_base_list = []
    count_muta_geno_list = []
    count_muta_base_list = []
    count_dgrec_geno_list = []
    count_dgrec_base_list = []
    geno_list_list = []

    # Iterate over each template in the dictionary
    for template in tqdm(dic_match):
        # Get the genotype statistics for the current template
        geno_list = get_genotype(template, dic_match[template])
        count_total_geno, count_total_base, count_muta_geno, count_muta_base, count_dgrec_geno, count_dgrec_base = get_mutation_percentage(geno_list)

        # Append the results to the respective lists
        TR_list.append(template)
        count_total_geno_list.append(count_total_geno)
        count_total_base_list.append(count_total_base)
        count_muta_geno_list.append(count_muta_geno)
        count_muta_base_list.append(count_muta_base)
        count_dgrec_geno_list.append(count_dgrec_geno)
        count_dgrec_base_list.append(count_dgrec_base)
        geno_list_list.append(geno_list)

    # Create a DataFrame from the collected lists
    df_muta_stats = pd.DataFrame(
        {
            'TR': TR_list,
            'count_total_geno': count_total_geno_list,
            'count_total_base': count_total_base_list,
            'count_muta_geno': count_muta_geno_list,
            'count_muta_base': count_muta_base_list,
            'count_dgrec_geno': count_dgrec_geno_list,
            'count_dgrec_base': count_dgrec_base_list,
            'geno_list': geno_list_list
        }
    )
    
    return df_muta_stats



def getting_percentage(df_stats):
    """
    Calculates percentage statistics for mutation and degenerate genotypes and bases.
    
    Parameters:
    df_stats : pandas.DataFrame
        A DataFrame containing mutation statistics with columns:
        - 'count_muta_geno': Number of mutated genotypes.
        - 'count_total_geno': Total number of genotypes.
        - 'count_muta_base': Number of mutated base positions.
        - 'count_total_base': Total number of base positions.
        - 'count_dgrec_geno': Number of degenerate genotypes.
        - 'count_dgrec_base': Number of degenerate base positions.
        
    Returns:
    pandas.DataFrame
        The input DataFrame with additional columns:
        - 'Percentage_muta_geno': Percentage of mutated genotypes.
        - 'Percentage_muta_base': Percentage of mutated base positions.
        - 'Percentage_dgrec_geno': Percentage of degenerate genotypes.
        - 'Percentage_dgrec_base': Percentage of degenerate base positions.
    """
    
    # Calculate the percentage of mutated genotypes and round it to 2 decimal places
    df_stats['Percentage_muta_geno'] = (100 * df_stats['count_muta_geno'] / df_stats['count_total_geno']).round(2)
    
    # Calculate the percentage of mutated base positions
    df_stats['Percentage_muta_base'] = (100 * df_stats['count_muta_base'] / df_stats['count_total_base'])
    
    # Calculate the percentage of degenerate genotypes and round it to 2 decimal places
    df_stats['Percentage_dgrec_geno'] = (100 * df_stats['count_dgrec_geno'] / df_stats['count_total_geno']).round(2)
    
    # Calculate the percentage of degenerate base positions
    df_stats['Percentage_dgrec_base'] = (100 * df_stats['count_dgrec_base'] / df_stats['count_total_base'])
    
    return df_stats  # Return the updated DataFrame



def find_mutations(seq1, seq2):
    """
    Identifies and lists mutations between two sequences by comparing corresponding positions.

    Parameters:
    - seq1 (str): The first sequence to compare.
    - seq2 (str): The second sequence to compare.

    Returns:
    - mutations (list of tuples): A list of mutations where each mutation is represented as a tuple (original_base, index, mutated_base).
      - original_base: The base from seq1.
      - index: The position of the mutation in the sequence.
      - mutated_base: The base from seq2.
    """

    # Initialize an empty list to store mutation information
    mutations = []

    # Iterate over each position in the sequences
    for i in range(len(seq1)):
        # Check if the bases at the current position are different and neither is a placeholder ('-')
        if seq1[i] != seq2[i] and seq1[i] != '-' and seq2[i] != '-':
            # Append a tuple of (original_base, index, mutated_base) to the mutations list
            mutations.append((seq1[i], i, seq2[i]))

    # Return the list of identified mutations
    return mutations


def get_genotype(ref_seq, molecule_list):
    """
    Analyzes a list of molecules to determine their genotypes relative to a reference sequence.

    Parameters:
    - ref_seq (str): The reference sequence to compare against.
    - molecule_list (list of str): A list of sequences (molecules) to analyze.

    Returns:
    - list_geno (list of tuples): A list of tuples where each tuple contains a genotype (mutation string) and its count.
      - The genotype is represented as a string of mutations.
      - The count is the number of occurrences of this genotype in the molecule_list.
    """
    
    # Initialize an empty list to store genotype strings
    mut_list = []

    # Iterate over each molecule in the molecule_list
    for molecule in molecule_list:
        # Find mutations between the reference sequence and the current molecule
        mutations = find_mutations(ref_seq, molecule)
        
        # Convert the mutations to a string format
        mut_str = dgrec.utils.mut_to_str(mutations)
        
        # Append the mutation string to the list if it contains fewer than 15 mutations
        if len(mut_str.split(',')) < 15:
            mut_list.append(mut_str)

    # Count the occurrences of each unique mutation string using Counter
    c = Counter(mut_list)
    
    # Convert the Counter object to a regular dictionary
    c = dict(c)

    # Initialize a list to store genotype counts
    list_geno = []

    # Create a list of tuples where each tuple contains a genotype and its count
    for geno in c:
        list_geno.append((geno, c[geno]))

    # Return the list of genotypes and their counts
    return list_geno



def bonferroni_correction(p_values):
    """
    Applies the Bonferroni correction to a list of p-values to control for multiple testing.
    
    Parameters:
    p_values : list of floats
        A list of p-values from multiple statistical tests.
        
    Returns:
    list of floats
        A list of Bonferroni-corrected p-values, where each p-value is adjusted for multiple comparisons.
    """
    
    # Number of tests (i.e., the number of p-values)
    num_tests = len(p_values)
    
    # Apply the Bonferroni correction to each p-value:
    # Multiply each p-value by the number of tests. If the result exceeds 1.0, set it to 1.0.
    corrected_p_values = [min(p * num_tests, 1.0) for p in p_values]
    
    # Return the list of corrected p-values
    return corrected_p_values



def p_value_to_stars(p_values):
    """
    Converts a list of p-values into star notation based on significance levels.
    
    Parameters:
    p_values : list of floats
        A list of p-values from statistical tests.
        
    Returns:
    list of strings
        A list of star notations corresponding to the significance levels of the p-values:
        - '****' for p <= 0.0001
        - '***' for p <= 0.001
        - '**' for p <= 0.01
        - '*' for p <= 0.05
        - 'ns' (not significant) for p > 0.05
    """
    
    # Initialize an empty list to store star notations
    star_notation = []
    
    # Loop over each p-value and assign the corresponding star notation
    for p in p_values:
        if p <= 0.0001:
            star_notation.append('****')  # Highly significant
        elif p <= 0.001:
            star_notation.append('***')   # Very significant
        elif p <= 0.01:
            star_notation.append('**')    # Significant
        elif p <= 0.05:
            star_notation.append('*')     # Marginally significant
        else:
            star_notation.append('ns')    # Not significant
    
    # Return the list of star notations
    return star_notation



# import ast

# def string_to_tuple_list(data_string):
#     # eval removes the outter brackets
#     data = ast.literal_eval(data_string)
#     return data


# def split_letters_numbers(geno):
#     return (geno[0],geno[1:-1],geno[-1])


# def count_A_muts(mut_list_split):
#     count_A=0
#     for mut in mut_list_split:
#         old_base=mut[0]
#         if old_base=='A':
#             count_A+=1
#     return count_A



def base_upstream_downstream(TR_list, genolist_list):
    """
    Extracts mutation information, including upstream and downstream base context, for a list of templates and their genotypes.
    
    Parameters:
    TR_list : list of str
        A list of template sequences (nucleotides) for each genotype.
        
    genolist_list : list of lists
        A list where each element is a list of genotypes. Each genotype is a tuple where:
        - The first element is a string with mutations (e.g., 'A3G').
        - The second element is an integer representing the UMI count (unique molecular identifier).
        
    Returns:
    dict
        A dictionary where each key corresponds to a specific feature (e.g., position, mutation type),
        and the values are lists of entries for each mutation in the input data. Features include:
        - 'TR': The template sequence.
        - 'UMI Count': The count for the genotype (how often it was observed).
        - 'Genotype': The specific genotype associated with the mutation.
        - 'Is_Genotype_DGR': Boolean indicating if the genotype is degenerate (as determined by is_geno_dgrec).
        - 'Old Base': The base that was mutated.
        - 'New Base': The base after mutation.
        - 'Position': The position of the mutation in the template.
        - Upstream and downstream bases around the mutation (-5 to +5 positions).
        - Whether adjacent bases (up to +3) were also mutagenized.
    """
    
    # Initialize a defaultdict to store the output data in a list format for each feature
    record_dic = defaultdict(list, {})
    
    # Loop over each template in TR_list and its corresponding genotype list
    for n in tqdm(range(len(TR_list))):
        TR = TR_list[n]  # Current template sequence
        genolist = genolist_list[n]  # List of genotypes for the current template
        
        # Iterate through each genotype in the list
        for k, geno_k in enumerate(genolist):
            # Extract mutations and UMI count
            mut_list = geno_k[0].split(',')  # Split genotype string by commas to get individual mutations
            pos_list = [k[1:-1] for k in mut_list]  # Extract positions of each mutation (ignoring the bases)
            count_geno = geno_k[1]  # UMI count for the current genotype
            
            # Iterate through each mutation in the genotype
            for i, mut in enumerate(mut_list):
                if mut != '':
                    old_base = mut[0]  # The original base
                    new_base = mut[-1]  # The mutated base
                    pos = int(mut[1:-1])  # The position of the mutation in the template
                    
                    # Ensure the position is not too close to the edges of the template
                    if pos > 5 and pos < (len(TR) - 5):
                        
                        # Append relevant mutation information to the dictionary
                        record_dic['TR'].append(TR)
                        record_dic['UMI Count'].append(count_geno)
                        record_dic['Genotype'].append(geno_k)
                        record_dic['Is_Genotype_DGR'].append(is_geno_dgrec(geno_k[0]))
                        record_dic['Old Base'].append(old_base)
                        record_dic['New Base'].append(new_base)
                        record_dic['Position'].append(pos)
                        
                        # Capture upstream and downstream bases (-5 to +5 around the mutation)
                        for k in range(-5, 6):
                            record_dic[str(k)].append(TR[pos + k])

                        # Check if the next three positions are also mutagenized
                        for j in range(1, 4):
                            pos_i_j = pos + j
                            if str(pos_i_j) in pos_list:
                                record_dic[f'Base +{j} mutagenized'].append(True)
                            else:
                                record_dic[f'Base +{j} mutagenized'].append(False)
    
    # Return the dictionary containing mutation data and context
    return record_dic
